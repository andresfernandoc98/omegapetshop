import { model, Schema, Types } from "mongoose";
import { UsuarioEntidad } from "../entidad/UsuarioEntidad";

const UsuarioEsquema = new Schema<UsuarioEntidad>({
    nombreUsuario: {type: String, required: true, trim: true},
    correoUsuario: {type: String, required: true, unique: true},
    claveUsuario: {type: String, required: true},
    fechaCreacionUsuario: { type: Date, default: Date.now()},
    estadoUsuario: {type: Number, enum: [1, 2, 3], default: 1},
    avatarUsuario: {type: String, default: "noAvatar"},
    codPerfil: {type: Types.ObjectId, ref:"Perfil", required:true},
},{versionKey: false});

export default model("Usuario", UsuarioEsquema, "Usuario");
