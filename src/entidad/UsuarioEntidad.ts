import { PerfilEntidad } from './PerfilEntidad';

export class UsuarioEntidad{
   public nombreUsuario: string;
   public correoUsuario: string;
   public claveUsuario: string;
   public fechaCreacionUsuario: Date;
   public estadoUsuario: number;
   public avatarUsuario: string;
   public codPerfil: PerfilEntidad;
   
   constructor(nomp: string, esta: number, corr: string, clav: string, fecr: Date, avat: string, codp: PerfilEntidad){
    this.nombreUsuario = nomp;
    this.correoUsuario = corr;
    this.claveUsuario = clav;
    this.fechaCreacionUsuario = fecr;
    this.estadoUsuario = esta;
    this.avatarUsuario = avat;
    this.codPerfil = codp;
   }  
};

export default UsuarioEntidad;