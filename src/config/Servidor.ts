import cors from "cors";
import morgan from "morgan";
import dotenv from "dotenv";
import express from "express";
import ConexionDB from "./ConexionDB";

// imports para ruteo
import rutaApiPerfil from "../ruta/PerfilRuta";
import rutaApiUsuario from "../ruta/UsuarioRuta";
import seguridad from "../middleware/Seguridad";

// *************************************//

class Servidor {
  public app: express.Application;

  constructor() {
    dotenv.config({ path: "variables.env" });
    ConexionDB();
    this.app = express();
    // cargar configuracion
    this.cargarConfiguracion();
    // activar rutas
    this.activarRutas();
  }

  public cargarConfiguracion() {
    this.app.set("PORT", process.env.PORT);
    this.app.use(cors());
    this.app.use(morgan("dev"));
    this.app.use(express.json({ limit: "100MB" }));
    this.app.use(express.urlencoded({ extended: true }));
  }

  public activarRutas(){
    this.app.use("/api/perfiles",seguridad.verificar, rutaApiPerfil);
    this.app.use("/api/usuarios", rutaApiUsuario);
  }

  public iniciarServidor(){
    this.app.listen(this.app.get("PORT"), ()=>{
      console.log("Servidor escuchando end", this.app.get("PORT"));
    });
  }
}

export default Servidor;
