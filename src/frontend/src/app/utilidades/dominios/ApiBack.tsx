const ApiBack = {
  URL: "http://localhost:3123",
  CREAR_USUARIO: "/api/usuarios/crear",
  INICIAR_SESION: "/api/usuarios/iniciar",

  PERFILES_OBTENER: "/api/perfiles/todos",
  PERFILES_CREAR: "/api/perfiles/crear",
  PERFILES_ELIMINAR: "/api/perfiles/eliminar",
  PERFILES_ACTUALIZAR: "/api/perfiles/actualizar",
  PERFILES_OBTENER_UNO: "/api/perfiles/uno"
};

export default ApiBack;